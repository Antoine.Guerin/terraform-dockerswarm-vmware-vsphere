provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server
  allow_unverified_ssl = true
  api_timeout = 3600
}
 
data "vsphere_datacenter" "dc" {
  name = var.data_center
}
data "vsphere_compute_cluster" "cluster" {
  name          = var.cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}
data "vsphere_datastore" "datastore" {
  name          = var.workload_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}
 
data "vsphere_resource_pool" "pool" {
  name          = var.compute_pool
  datacenter_id = data.vsphere_datacenter.dc.id
}
 
data "vsphere_host" "host" {
  name          = var.host
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = var.vsphere_network
  datacenter_id = data.vsphere_datacenter.dc.id
}

##############################################
######### Deploiment de la VM01 ##############
##############################################

resource "vsphere_virtual_machine" "Docker01" {
  name                 = "${var.name_vm01}"
  datacenter_id        = data.vsphere_datacenter.dc.id
  datastore_id         = data.vsphere_datastore.datastore.id
  host_system_id       = data.vsphere_host.host.id
  folder               = var.vsphere_folder
  resource_pool_id     = data.vsphere_resource_pool.pool.id
  num_cpus             = var.num_cpus_vm
  num_cores_per_socket = var.num_cores_per_socket_vm
  memory               = var.memory_vm
  guest_id             = var.guest_id_vm
  firmware             = var.firmware_vm
  hardware_version     = var.hardware_version_vm
  scsi_type            = var.scsi_type_vm
  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = "vmxnet3"
  }

  ### Remplace la taille du disque par defaut
  disk { 
    label = "disk0"
    unit_number = 0
    size = var.size_disk_vm
  }

  wait_for_guest_net_timeout = 0
  wait_for_guest_ip_timeout  = 0

  ovf_deploy {
    allow_unverified_ssl_cert = false
    local_ovf_path            = var.ovf_local
    disk_provisioning         = "thin"
    ip_protocol               = "IPV4"
    ip_allocation_policy      = "STATIC_MANUAL"
    ovf_network_map = {
      "vmxnet3" = data.vsphere_network.network.id
    }
  }

  extra_config = {
    "guestinfo.hostname"                      = var.name_vm01
    "guestinfo.ignition.config.data.encoding" = "base64"
    "guestinfo.ignition.config.data"          = base64encode(file("./template/ignition_01.json"))
    "guestinfo.afterburn.initrd.network-kargs" = "ip=${var.ip_address_vm01}::${var.gateway_vm}:${var.netmask_vm}:${var.name_vm01}:ens192:off:${var.dns1_vm}:${var.dns2_vm}"
  }

}

##############################################
######### Deploiment de la VM02 ##############
##############################################

resource "vsphere_virtual_machine" "Docker02" {
  name                 = "${var.name_vm02}"
  datacenter_id        = data.vsphere_datacenter.dc.id
  datastore_id         = data.vsphere_datastore.datastore.id
  host_system_id       = data.vsphere_host.host.id
  folder               = var.vsphere_folder
  resource_pool_id     = data.vsphere_resource_pool.pool.id
  num_cpus             = var.num_cpus_vm
  num_cores_per_socket = var.num_cores_per_socket_vm
  memory               = var.memory_vm
  guest_id             = var.guest_id_vm
  firmware             = var.firmware_vm
  hardware_version     = var.hardware_version_vm
  scsi_type            = var.scsi_type_vm
  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = "vmxnet3"
  }

  ### Remplace la taille du disque par defaut
  disk { 
    label = "disk0"
    unit_number = 0
    size = var.size_disk_vm
  }

  wait_for_guest_net_timeout = 0
  wait_for_guest_ip_timeout  = 0

  ovf_deploy {
    allow_unverified_ssl_cert = false
    local_ovf_path            = var.ovf_local
    disk_provisioning         = "thin"
    ip_protocol               = "IPV4"
    ip_allocation_policy      = "STATIC_MANUAL"
    ovf_network_map = {
      "vmxnet3" = data.vsphere_network.network.id
    }
  }

  extra_config = {
    "guestinfo.hostname"                      = var.name_vm02
    "guestinfo.ignition.config.data.encoding" = "base64"
    "guestinfo.ignition.config.data"          = base64encode(file("./template/ignition_02.json"))
    "guestinfo.afterburn.initrd.network-kargs" = "ip=${var.ip_address_vm02}::${var.gateway_vm}:${var.netmask_vm}:${var.name_vm02}:ens192:off:${var.dns1_vm}:${var.dns2_vm}"
  }

}

##############################################
######### Deploiment de la VM03 ##############
##############################################

resource "vsphere_virtual_machine" "Docker03" {
  name                 = "${var.name_vm03}"
  datacenter_id        = data.vsphere_datacenter.dc.id
  datastore_id         = data.vsphere_datastore.datastore.id
  host_system_id       = data.vsphere_host.host.id
  folder               = var.vsphere_folder
  resource_pool_id     = data.vsphere_resource_pool.pool.id
  num_cpus             = var.num_cpus_vm
  num_cores_per_socket = var.num_cores_per_socket_vm
  memory               = var.memory_vm
  guest_id             = var.guest_id_vm
  firmware             = var.firmware_vm
  hardware_version     = var.hardware_version_vm
  scsi_type            = var.scsi_type_vm
  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = "vmxnet3"
  }

  ### Remplace la taille du disque par defaut
  disk { 
    label = "disk0"
    unit_number = 0
    size = var.size_disk_vm
  }

  wait_for_guest_net_timeout = 0
  wait_for_guest_ip_timeout  = 0

  ovf_deploy {
    allow_unverified_ssl_cert = false
    local_ovf_path            = var.ovf_local
    disk_provisioning         = "thin"
    ip_protocol               = "IPV4"
    ip_allocation_policy      = "STATIC_MANUAL"
    ovf_network_map = {
      "vmxnet3" = data.vsphere_network.network.id
    }
  }

  extra_config = {
    "guestinfo.hostname"                      = var.name_vm03
    "guestinfo.ignition.config.data.encoding" = "base64"
    "guestinfo.ignition.config.data"          = base64encode(file("./template/ignition_03.json"))
    "guestinfo.afterburn.initrd.network-kargs" = "ip=${var.ip_address_vm03}::${var.gateway_vm}:${var.netmask_vm}:${var.name_vm03}:ens192:off:${var.dns1_vm}:${var.dns2_vm}"
  }

}

##############################################
######### Deploiment de la VM04 ##############
##############################################

resource "vsphere_virtual_machine" "Docker04" {
  name                 = "${var.name_vm04}"
  datacenter_id        = data.vsphere_datacenter.dc.id
  datastore_id         = data.vsphere_datastore.datastore.id
  host_system_id       = data.vsphere_host.host.id
  folder               = var.vsphere_folder
  resource_pool_id     = data.vsphere_resource_pool.pool.id
  num_cpus             = var.num_cpus_vm
  num_cores_per_socket = var.num_cores_per_socket_vm
  memory               = var.memory_vm
  guest_id             = var.guest_id_vm
  firmware             = var.firmware_vm
  hardware_version     = var.hardware_version_vm
  scsi_type            = var.scsi_type_vm
  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = "vmxnet3"
  }

  ### Remplace la taille du disque par defaut
  disk { 
    label = "disk0"
    unit_number = 0
    size = var.size_disk_vm04
  }

  wait_for_guest_net_timeout = 0
  wait_for_guest_ip_timeout  = 0

  ovf_deploy {
    allow_unverified_ssl_cert = false
    local_ovf_path            = var.ovf_local
    disk_provisioning         = "thin"
    ip_protocol               = "IPV4"
    ip_allocation_policy      = "STATIC_MANUAL"
    ovf_network_map = {
      "vmxnet3" = data.vsphere_network.network.id
    }
  }

  extra_config = {
    "guestinfo.hostname"                      = var.name_vm04
    "guestinfo.ignition.config.data.encoding" = "base64"
    "guestinfo.ignition.config.data"          = base64encode(file("./template/ignition_04.json"))
    "guestinfo.afterburn.initrd.network-kargs" = "ip=${var.ip_address_vm04}::${var.gateway_vm}:${var.netmask_vm}:${var.name_vm04}:ens192:off:${var.dns1_vm}:${var.dns2_vm}"
  }

}