vsphere_user            = "administrator@vsphere.local"
vsphere_password        = "Monsupermotdepasse"
vsphere_server          = "vcenter.mydomain.fr"
vsphere_folder          = "/Prod/Infra/Docker"

data_center             = "Datacenter"
cluster                 = "Mycluster"
workload_datastore      = "SSD2TO"
compute_pool            = "DockerSwarm"
host                    = "223.223.223.252"

vsphere_network         = "LAN"
ovf_local               = "./ova/fedora-coreos-38.20231027.3.2-vmware.x86_64.ova"

num_cpus_vm             = "2"
num_cores_per_socket_vm = "2"
memory_vm               = "2048"
guest_id_vm             = "otherGuest64"
firmware_vm             = "efi"
hardware_version_vm     = "20"
scsi_type_vm            = "pvscsi"
size_disk_vm            = 20

name_vm01               = "Docker01"
ip_address_vm01         = "223.223.223.101"

name_vm02               = "Docker02"
ip_address_vm02         = "223.223.223.102"

name_vm03               = "Docker03"
ip_address_vm03         = "223.223.223.103"

name_vm04               = "DockerMinio"
ip_address_vm04         = "223.223.223.100"
size_disk_vm04          = 150

gateway_vm              = "223.223.223.254"
netmask_vm              = "255.255.255.0"
dns1_vm                 = "223.223.223.254"
dns2_vm                 = "223.223.223.254"