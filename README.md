# Déploiement de VMs Fedora CoreOS via Terraform sur Vmware Vsphere

Dans cet exemple, sera déployé 4 VMs. 3 VMs pour DockerSwarm et 1 VM pour héberger un serveur Minio qui accueillera les données persistantes. 

Cet exemple se base sur un template OVA de Fedora CoreOS.

### Commencer
- Créer un dossier **ova** à la racine, puis y déposer le fichier OVA [Télécharger OVA](https://fedoraproject.org/coreos/download?stream=stable) dans celui-ci.
- Modifier le fichier **terraform.tfvars** avec vos paramètres vsphere, cpu, network etc...
- Modifier la ligne `"contents": { "source": "data:,hostname" }` dans les fichiers json **ignition** situés dans le dossier "template" pour modifier les noms hostname de vos VMs.
- Toujours dans les fichiers json **ignition**, vous pouvez modifier le mot de passe root (Dans cet exemple le mot de passe déclaré est: test). Le mot de passe doit être hashé, pour générer un hashage de mot de passe sécurisé, utilisez la commande mkpasswd: `mkpasswd --method=yescrypt`.
- Toujours dans les fichiers json **ignition**, vous pouvez supprimer ou modifier la clé SSH autorisée. Si vous n'avez pas de clé SSH, vous pouvez suivre ce [tuto](https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key).

### Démarrage

- `terraform init` pour initialiser Terraform
- `terraform plan` pour vérifier votre configuration Terraform
- `terraform apply --auto-approve` pour déployer les VMs

#### Si tout fonctionne correctement, après quelques instants vous devriez avoir vos 4 VMs déployées :)